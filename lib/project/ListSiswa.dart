import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'dart:convert';
import 'dart:async';

import 'package:sekolahku/project/FormAddSiswa.dart';
import 'package:sekolahku/project/Login.dart';
import 'package:sekolahku/project/ViewSiswa.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ListSiswa extends StatefulWidget {


  @override 
  _ListSiswaState createState() => _ListSiswaState();

}

class _ListSiswaState extends State<ListSiswa> {

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  @override
  void initState() {
    super.initState();
    getData();
  }

  // mengambil da ta dari database
  Future<List> getData() async {
    // refresh
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 1));
    // data
    final response = await http.get("http://10.0.2.2/flutter-sekolahku-server/siswa/getsiswa.php");
    return json.decode(response.body);

  } 

  Widget build(BuildContext context){
    
    return Scaffold(
      appBar: AppBar(
        title: Text("List Siswa"),
        leading: Icon(Icons.school),
        // tombol logout
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.lock_open, semanticLabel: "Logout",),

            onPressed: () async {
              // hapus shared prefs login
              final prefs = await SharedPreferences.getInstance();
              prefs.remove('login');
              // redirect page/route ke login
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Login()),
              );
            },
          ),
        ],
      ),
      body: RefreshIndicator(
        key: refreshKey,
        child: FutureBuilder<List>(
          future: getData(),
          builder: (context, snapshot){
            // cek apakah ada error atau tidak
            if(snapshot.hasError) print(snapshot.error);
            // jika tidak terjadi error dan ada data
            return snapshot.hasData
            ? new ItemList(
              list: snapshot.data,
            )
            : Center(
              child:  CircularProgressIndicator(),
            );
          },
        ),
        onRefresh: getData,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => AddSiswa(),
          )
        ),
      ),
    );
  }
}

class ItemList extends StatelessWidget{
  final List list;
  ItemList({this.list});
  @override
  Widget build(BuildContext context){
    return new ListView.builder(
      itemCount:  list == null ? 0 : list.length,
      itemBuilder: (context, i){
        return new Container (
          padding: const EdgeInsets.all(5.0),
          child: new GestureDetector(
            // ke halaman detail jika di klik
            onTap: () => Navigator.of(context).pushReplacement(
              new MaterialPageRoute(
                builder: (BuildContext context) => ViewSiswa(list:list, index: i,)
              ),
            ),
            child: Card(
              child: new ListTile(
                title: Text(list[i]['nama']),
                leading: Icon(Icons.person),
                subtitle:  Text("Usia : ${list[i]['usia']} tahun"),
              )
            ),
          ),
        );
      },
    );
  }
}