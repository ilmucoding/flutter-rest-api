import 'package:flutter/material.dart';
import 'package:sekolahku/project/ListSiswa.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class Response {

  final String status;
  final String message;

  Response({this.status, this.message});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      status: json['status'],
      message: json['message'],
    );
  }
  
}

class Login extends StatefulWidget {
  
  @override
  LoginState createState() => new LoginState();
}
 
class LoginState extends State<Login> {

  bool _secureText = true;
  showHide() {
    setState(() {
      _secureText = !_secureText;
    });
  }


  // variabel member class
  final _email = TextEditingController();
  final _password = TextEditingController();

  // variabel response
  String _response = "";
  bool _apiCall = false;

  // status login menggunakan shared preference
  bool alreadyLogin = false;

  // function untuk kirim http post
  Future<Response> post(String url, var body) async {

    return await http
        .post(Uri.encodeFull(url), body: body, headers: {"Accept" : "application/json"})
        .then((http.Response response) {

          final int statusCode = response.statusCode;

          if(statusCode < 200 || statusCode > 400 || json == null){
            throw new Exception("Error while fetchingdata");
          }
          return Response.fromJson(json.decode(response.body));
        });
  }

  // function untuk kirim email dan password, lalu dicek loginnya
  void _toLogin() {
    post(
      'http://10.0.2.2/flutter-sekolahku-server/login.php',
      {
        'email' : _email.text,
        'password' : _password.text,
      }).then((response) async {
        
        setState(() {
          _apiCall = false;
          _response = response.message;
        });

        // cek jika login berhasil
        if(response.status == "success") {

          // beri tanda sudah login
          final prefs = await SharedPreferences.getInstance();
          
          setState(() {
            alreadyLogin = true;
            prefs.setBool('login', alreadyLogin);
          });

          // langsung menuju list siswa jika sudah pernah login
          Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => ListSiswa())
          );
        }
      },
      
      // jika response error
      onError: (error){
        _apiCall = false;
        _response = error.toString();
      }
      
    );
  }

  // Menampilkan progress login

  Widget progressWidget() {

    if(_apiCall)

      // jika masih proses kirim API
      return AlertDialog(
        content: new Column(
          children: <Widget>[
            CircularProgressIndicator(),
            Text("Please Wait")
          ],
        ),
      );
    
    else

      // jika sudah selesai kirim API
      return Center(
        child: Text(
          _response,
          style: TextStyle(fontSize: 15.0),
        ),
      );
  }
  
  // ambil status login
  Future<bool> getLoginStatus() async{
    final prefs = await SharedPreferences.getInstance();
    bool loginStatus = prefs.getBool('login') ?? false;
    return loginStatus;
  }
 
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
        future: getLoginStatus(),
        builder: (context, snapshot) {
          return (snapshot.data) ?
          // jika sudah login tampilkan list siswa
          new ListSiswa() :
          // jika belum login tampilkan form login
          loginForm();
        },
      );
  }
 
  @override
  Widget loginForm() {
    return Scaffold(
      body: Center(
          child: ListView(
            shrinkWrap: true,
            padding: EdgeInsets.only(left: 24.0, right: 24.0),
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 25.0),
              ),
              Hero(
                tag: 'hero',
                child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  radius: 48.0,
                  child: Image.asset('images/logo2.png'),
                ),
              ),
              SizedBox(height: 24.0),
              Center(
                child: Column(
                  children: <Widget>[
                    Text(
                      "SEKOLAHKU",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.grey),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 24.0),
              TextFormField(
                controller: _email,
                keyboardType: TextInputType.text,
                autofocus: false,
                decoration: InputDecoration(
                  hintText: 'email',
                  filled: true,
                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                  suffixIcon: Icon(Icons.person),
                ),
              ),
              // spasi
              SizedBox(height: 8.0),
              TextFormField(
                autofocus: false,
                obscureText: _secureText,
                controller: _password,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: 'Password',
                  filled: true,
                  contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
                  suffixIcon: IconButton(
                     onPressed: showHide,
                     icon: Icon(_secureText
                         ? Icons.visibility_off
                         : Icons.visibility),
                  ),
                ),
              ),
              // spasi
              SizedBox(height: 24.0),
              // tombol
              Padding(
                padding: EdgeInsets.symmetric(vertical: 16.0),
                child: Material(
                  borderRadius: BorderRadius.circular(30.0),
                  shadowColor: Colors.lightBlueAccent.shade100,
                  elevation: 5.0,
                  child: MaterialButton(
                    minWidth: 200.0,
                    height: 42.0,
                    onPressed: () {
                      setState(() {
                        _apiCall = true;
                      });
                      _toLogin();
                    },
                    color: Colors.lightBlueAccent,
                    child: Text('Log In', style: TextStyle(color: Colors.white, fontSize: 20.0)),
                  ),
                ),
              ),
              progressWidget()
            ],
          )
      )
    );
  }
}